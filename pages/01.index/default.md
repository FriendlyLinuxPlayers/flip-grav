---
title: Home
metadata:
  description: 'FLiP is an inclusive online gaming community of Linux enthusiasts.'
  keywords: 'Linux, Gaming, Community'
  color-scheme: 'dark'
  'og:title': 'Welcome to the Friendly Linux Players community!'
  'og:type': 'website'
  'og:image': 'https://friendlylinuxplayers.org/user/themes/flip/images/logo/flip-logo-full-bg-dark.png'
  'og:image:alt': 'The Friendly Linux Players logo'
  'og:url': 'https://friendlylinuxplayers.org'
  'og:description': 'FLiP is an inclusive online gaming community of Linux enthusiasts.'
  'og:site_name': 'Friendly Linux Players'
---

# Welcome to the Friendly Linux Players community!

## About Us

<abbr title="Friendly Linux Players">FLiP</abbr> is an inclusive online gaming community of Linux enthusiasts. We aim to be welcoming to all who would want to join us, provided our [Code of Conduct](/conduct) is adhered to.

There are no requirements regarding Linux usage or experience. Our members include seasoned desktop Linux gamers, those who just received a new [Steam Deck](https://www.steamdeck.com), and Windows users who have interest in a friendly and inclusive community.

Our hosted services (Matrix and Mumble) serve as social centers for the community, including for our regularly-hosted gaming events.

## Join Us

There are multiple places to participate in the Friendly Linux Players community:

* [Matrix](https://element.flip.earth/#/room/%23flip:flip.earth) (text chat)  
  This links to our web-based text chat, specifically to the main room where the community is centered. For other Matrix clients, this room is at [#flip:flip.earth](https://matrix.to/#/%23flip:flip.earth). Additionally, join the community space to better discover and organize all of our official rooms: [#community:flip.earth](https://matrix.to/#/#community:flip.earth)
* [Mumble](mumble://mumble.flip.earth) (voice chat)  
  This is available at [mumble.flip.earth](mumble://mumble.flip.earth), using the default port (64738). Note that self-registering allows the creation of temporary channels.
* [Steam](https://steamcommunity.com/groups/FriendlyLinuxPlayers)  
  Joining our Steam group is strictly optional. Many Steam community features have been disabled, so as to focus on our Matrix space and Mumble server.
* [GitLab](https://gitlab.com/FriendlyLinuxPlayers) (code repositories)

By participating in our community, you agree to abide by the terms of our [Code of Conduct](/conduct).

If you notice a violation of the Code of Conduct or have any concerns, please contact someone from our team of moderators via a private message on Matrix:

* DerRidda ([@DerRidda:flip.earth](https://matrix.to/#/@DerRidda:flip.earth))
* HER0 ([@HER0:matrix.org](https://matrix.to/#/@HER0:matrix.org))
* Lara Flynn ([@cat5e:matrix.org](https://matrix.to/#/@cat5e:matrix.org))
* Mohero ([@Mohero:flip.earth](https://matrix.to/#/@Mohero:flip.earth))
* Jay C. ([@JayCee98:matrix.org](https://matrix.to/#/@JayCee98:matrix.org))

Incidents can also be reported via email at [conduct@flip.earth](mailto:conduct@flip.earth).

### Events

Anyone in the community can host a gaming event, which anyone else is welcome to attend. To learn more, see the [Events page](/events).
