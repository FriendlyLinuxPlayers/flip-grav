---
title: Privacy Statement
metadata:
  description: 'The Privacy Statement for the FLiP website.'
  keywords: 'Linux, Gaming, Community, Privacy, Policy, Cookie'
  color-scheme: 'dark'
  'og:title': 'Privacy Statement'
  'og:type': 'website'
  'og:image': 'https://friendlylinuxplayers.org/user/themes/flip/images/logo/flip-logo-full-bg-dark.png'
  'og:image:alt': 'The Friendly Linux Players logo'
  'og:url': 'https://friendlylinuxplayers.org/privacy'
  'og:description': 'The Privacy Statement for the FLiP website.'
  'og:site_name': 'Friendly Linux Players'
---

# The Friendly Linux Players Website Privacy Statement

Thank you for caring about privacy! We do so as well.

 This website comes without any kind of tracking. Heck, we do not even use any cookies at all, but there is one thing we have to tell you about.

## Web Server Logfiles

* **What is being collected?**

  With every visit to one of our pages our web server automatically collects the following information.

  1. Your IP-address
  2. The time and date of access
  3. The HTTP method used for the request
  4. The requested path on the server
  5. The HTTP version used for that request
  6. The HTTP status code for the response
  7. The number of transmitted bytes
  8. The referral site
  9. Your browser's user agent string containing information about your browser and its version as well as your operating system

* **What is the collected data used for?**

  The collected information is fairly standard for just about every web server and is only used to ensure the proper operation of our website as it enables us to track errors and detect attempted attacks on our infrastructure. 

  We conduct no further analysis of the collected data.

* **How long is the data kept for?**

  The collected data will be automatically deleted after seven days.
