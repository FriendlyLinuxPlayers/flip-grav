---
title: Events
metadata:
  description: 'Join us in our community gaming events!'
  keywords: 'Linux, Gaming, Event, Community'
  color-scheme: 'dark'
  'og:title': 'Events with Friendly Linux Players'
  'og:type': 'website'
  'og:image': 'https://friendlylinuxplayers.org/user/themes/flip/images/logo/flip-logo-full-bg-dark.png'
  'og:image:alt': 'The Friendly Linux Players logo'
  'og:url': 'https://friendlylinuxplayers.org/events'
  'og:description': 'Join us in our community gaming events!'
  'og:site_name': 'Friendly Linux Players'
cache_control: max-age=60, must-revalidate
---

# Events with Friendly Linux Players

Join us in our community gaming events!

There are a number of ways to learn about upcoming events, which can be hosted by any volunteer within the community. Like the rest of the community, events are centered around our main Matrix room ([#flip:flip.earth](https://matrix.to/#/#flip:flip.earth)).

## Discovering events

To learn about upcoming events, there are several options:

1. Upcoming events are listed at the bottom of this page.
1. Join the main Matrix room and send the following message to query our bot for upcoming events: `!f events`  
   In addition, the bot will send reminders to the room whenever an event is created, at certain fixed times before the event occurs, and at the scheduled time of the event.
1. Subscribe to the calendar feed with your favorite calendaring application (such as Thunderbird, Evolution, Google Calendar, Proton Calendar, iCal, etc.): [webcal://flip.earth/feed/events.ics](webcal://flip.earth/feed/events.ics)
1. At this time, the [Steam group](https://steamcommunity.com/groups/FriendlyLinuxPlayers) might not always have the most up-to-date event information. [This might change in the future](https://gitlab.com/FriendlyLinuxPlayers/bots/flip-matrix-bot/-/issues/23). Historically, most scheduled events have been posted there, but it is best to use the above methods to learn of upcoming events.

In the future, events will be posted to our Mastodon account ([@flip@masto.ai](https://masto.ai/@flip)).

## Joining events

If you have interest in joining our events, and can abide by the terms of our [Code of Conduct](/conduct), you are welcome! There are no other requirements, so you needn't worry about skill level, experience, or operating system. For the most part, you can just show up at the scheduled place and time, ready to play the game.

Most events will take place in our [Mumble](https://www.mumble.info/) server, which provides voice chat. Our server is available at [mumble.flip.earth](mumble://mumble.flip.earth) and the default port (64738).

If you need help, or for further event coordination, talk to us in Matrix ([#flip:flip.earth](https://matrix.to/#/#flip:flip.earth))! If you don't have Matrix already, the quickest way to join is with our <a href=https://element.flip.earth>hosted web client</a>. To learn more about Friendly Linux Players and how to participate in the community, visit the [Home page](/).

## Scheduling events

Anyone who would like to volunteer to host an event can schedule one using our Matrix bot. To get started with this, join the main Matrix room and send the following message: `!f events help add`
