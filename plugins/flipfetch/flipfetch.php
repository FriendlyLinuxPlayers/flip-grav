<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Grav;
use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

class FlipFetchPlugin extends Plugin
{
    public static function getSubscribedEvents(): array
    {
        return [
            'onTwigInitialized' => [
                ['onTwigInitialized', 0]
            ]
        ];
    }

    public function onTwigInitialized(Event $e)
    {
        $this->grav['twig']->twig()->addFilter(
            new \Twig_SimpleFilter('preview', [$this, 'preview'])
        );
    }

    public function preview($url)
    {
        $key = 'flipfetch-' . base64_encode($url);
        $cache = Grav::instance()['cache'];
        if ($cache->contains($key)) {
            return($cache->fetch($key));
        }
        try {
            $html = file_get_contents($url);
            $doc = new \DOMDocument();
            @$doc->loadHTML($html);
            $xpath = new \DOMXPath($doc);
            $query = '//*/meta[starts-with(@property, \'og:image\')]';
            $images = $xpath->query($query);
            $first = $images->item(0)->getAttribute('content');
            if (!parse_url($first, PHP_URL_HOST)) {
                $urldata = parse_url($url);
                $first = "$urldata[scheme]://$urldata[host]$first";
            }
            $cache->save($key, $first, 604800);
            return($first);
        } catch (\Throwable $e) {
            return("");
        }
    }
}
