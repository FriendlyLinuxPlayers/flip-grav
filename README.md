# flip-grav

The current website for the [Friendly Linux Players](https://friendlylinuxplayers.org) community. This is built off of the [Grav](https://getgrav.org/) CMS.

## Usage

You can test this out locally by placing these files into the `user` directory of a Grav install. The production instance only uses one plugin: [sitemap](https://github.com/getgrav/grav-plugin-sitemap).

## How does it work

`pages` contains the data used to build each webpage of the site. This includes displaying community events ([live example here](https://friendlylinuxplayers.org/events/cab3ab22148be33a)). Within `pages/02.events/flip-matrix-bot` (a directory that is not committed to this repository), it is intended that [flip-matrix-bot](https://gitlab.com/FriendlyLinuxPlayers/bots/flip-matrix-bot) will write event metadata using the `--website-path` option.

These dynamic event pages rely on Twig templates in `themes` and a tiny bit of PHP code in `plugins`.

You can learn more about the folder layout in the [Grav documentation](https://learn.getgrav.org/16/basics/folder-structure#user).
